package net.larboard.service.discovery.authentication.data;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

@Getter
@Setter
public class AuthUser extends User {
    public AuthUser(String username, String token, Collection<? extends GrantedAuthority> authorities) {
        super(username, token, authorities);
    }
}
