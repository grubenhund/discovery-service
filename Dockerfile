FROM openjdk:11-jre-slim

ENTRYPOINT ["java", "-jar", "/usr/share/app.jar"]

# Add the service itself
ARG JAR_FILE
ADD target/${JAR_FILE} /usr/share/app.jar